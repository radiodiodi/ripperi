FROM alpine
RUN apk add --no-cache ffmpeg
RUN apk add --no-cache bash
RUN apk add --no-cache curl
RUN apk add --no-cache jq

COPY rip.sh rip.sh
CMD bash rip.sh
