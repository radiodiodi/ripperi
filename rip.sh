#!/bin/bash

TIMEOUT=3600 # seconds

function rip () {
    read min sec <<< $(date +'%M %S')
    until=$(( $TIMEOUT - 10#$min*60 - 10#$sec + 10 ))
    until=`date -d @$until -u +%H:%M:%S`
    now_date=`date "+%Y-%m-%d-%H-%M-%S"`
    filepath=$REF_FOLDER/ref_$1_${now_date}.opus
    echo Saving $filepath ...
    ffmpeg -i $STREAM_URL -t $until $filepath
}

while true; do
    title=`curl -s ${INFO_URL} | jq .title | tr -d '"' | tr ' ' '_'`
    rip "${title}"
done
